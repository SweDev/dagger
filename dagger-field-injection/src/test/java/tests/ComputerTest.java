package tests;

import static org.assertj.core.api.Assertions.assertThat;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;

import components.ComputerComponent;
import components.DaggerComputerComponent;
import domain.Computer;

public class ComputerTest {

    @Inject
    Computer computer;

    @Test
    void test_computer_ok() {

        ComputerComponent component = DaggerComputerComponent.create();
        component.inject(this);

        assertThat(computer).isNotNull();
        assertThat(computer.getMotherboard().getPrice()).isEqualTo(10);

    }

}