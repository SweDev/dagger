package components;

import dagger.Component;

@Component
public interface ComputerComponent {

    void inject(ComputerTest computerTest);
}