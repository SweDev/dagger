package domain;

import javax.inject.Inject;

public class Memory {
    int price;

    @Inject
    public Memory() {
        this.price = 5;
    }

    public int getPrice() {
        return this.price;
    }
}
