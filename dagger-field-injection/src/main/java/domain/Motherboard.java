package domain;

import javax.inject.Inject;

public class Motherboard {

    int price;

    @Inject
    public Motherboard() {
        this.price = 10;
    }

    public int getPrice() {
        return this.price;
    }

}
