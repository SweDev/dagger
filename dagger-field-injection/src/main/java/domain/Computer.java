package domain;

import javax.inject.Inject;

public class Computer {

    private Motherboard motherboard;
    private Memory memory;
    private Hardrive hardrive;

    @Inject
    public Computer(Motherboard motherboard, Memory memory, Hardrive hardrive) {
        this.motherboard = motherboard;
        this.memory = memory;
        this.hardrive = hardrive;
    }

    public Motherboard getMotherboard() {
        return this.motherboard;
    }

    public Memory getMemory() {
        return this.memory;
    }

    public Hardrive getHardrive() {
        return this.hardrive;
    }

}
