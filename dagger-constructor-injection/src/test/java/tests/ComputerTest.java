package tests;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import components.ComputerComponent;
import components.DaggerComputerComponent;
import domain.Computer;

public class ComputerTest {

    ComputerComponent component = DaggerComputerComponent.create();
    Computer computer = component.getComputer();

    @Test
    void test_computer_ok() {

        assertThat(computer).isNotNull();
        assertThat(computer.getMotherboard().getPrice()).isEqualTo(10);

    }

}