package domain;

import javax.inject.Inject;

public class Hardrive {

    int price;

    @Inject
    public Hardrive() {
        this.price = 1;
    }

    public int getPrice() {
        return this.price;
    }
}
