package components;

import dagger.Component;
import domain.Computer;

@Component
public interface ComputerComponent {

    Computer getComputer();

}